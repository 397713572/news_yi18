
Ext.define('NewsApp.store.SearchListStore',{
	extend:'Ext.data.Store',
	config: {
		 model: 'NewsApp.model.SearchModel',
	
        //filter the data using the firstName field
       
            //autoload the data from the server
			
       		autoLoad: false,
        	 proxy: {
               		 type: 'jsonp',
              	 	 url: Global.Website+'news/search',
               		 reader: {
				            type: "json",
				            rootProperty: "yi18"
				        }
           	 }
					

	}
});