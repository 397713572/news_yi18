 Ext.define('NewsApp.model.NewsModel', {
     extend: 'Ext.data.Model',
     config: {
     	
     	

    	 fields: [ 
    	 	{name: 'id', type: 'auto'},
    	    {name: 'title', type: 'string'}, 
    	    {name: 'tag', type: 'string'},
    	   {name: 'count', type: 'int'},
    	   {name:'message',type:'string'},
    	  {name:'img',type:'string'},
    	  {name:'time',type:'date'},
    	  {name:'focal',type:'int'},
    	  {name:'author',type:'string'}
    	 ]
     }
 });