
/**
 * 新闻页面
 */
Ext.define('NewsApp.view.news.NewsShow', 
{
	extend: 'Ext.DataView',
	xtype: 'newsshow',
	
    config: {
        	
 			 
			
			    itemTpl: '<br><div style="font-size:12pt;font-weight: bold;" align="center">{title}</div>' +
			    		'<div style="color:#666;font-size:10pt;" align="center">{author} 分类: {tag} ({count}阅读)</div>' +
			    		'<HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="98%" color=#aaa SIZE=2>' +
			    		'<div>{message}</div>'
        }
        
  
	
});