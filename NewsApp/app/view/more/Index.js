

/**
 * 更多分类
 */
Ext.define('NewsApp.view.more.Index', 
{
	extend: 'Ext.navigation.View',
	xtype: 'more',

    config: {
        	defaultBackButtonText :'返回',
        	items:[
        			
        			{
        				title:Global.Title+'—更多分类',
        			   xtype:'classlist'
        			   
        			}]
        
        }
	
});